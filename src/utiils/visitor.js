import * as inflection from "inflected";
import { printSchemaWithDirectives } from "@graphql-tools/utils";
import { parse, visit, printSchema } from "graphql";
import * as inflected from "inflected";
import { singularize } from "inflected";
import { underscore } from "inflected";

export const extractData = (schema) => {
  let models = {};
  let embeddedModels = {};
  let abstracts = {};
  let inherited = {};

  const getType = (type) => {
    if (type.kind === "NonNullType") {
      return `${getType(type.type)}!`;
    } else if (type.kind === "NamedType") {
      return type.name.value;
    } else if (type.kind === "ListType") {
      return `[${getType(type.type)}]`;
    }
    return "";
  };

  const getDirective = (directive, node) => {
    const _directive = node.directives.find((d) => d.name.value === directive);
    if (!_directive) {
      return null;
    }
    return _directive.arguments.reduce((agg, a) => {
      agg[a.name.value] = a.value.value;
      return agg;
    }, {});
  };

  const getFieldDirective = (directive, field) => {
    const found = (field.directives || []).find(
      (d) => d.name.value === directive
    );
    if (found) {
      return found.arguments.reduce((a, arg) => {
        return { ...a, [arg.name.value]: arg.value.value };
      }, {});
    }
  };

  const getFields = (node) => {
    const type = schema._typeMap[node.name.value];
    return {
      ...Object.entries(node.fields).reduce((fields, [fieldName, field]) => {
        const mongoInfo = getFieldDirective("mongo", field);
        const {
          name: db_name = field.name.value === "id"
            ? "_id"
            : inflection.underscore(field.name.value),
        } = mongoInfo || {};
        fields[field.name.value] = {
          type: getType(field.type),
          db_name,
          ...(mongoInfo || {}),
        };
        return fields;
      }, {}),
    };
  };

  const isInheritDescendant = (node) => {
    const mongoInfo = getDirective("mongo", node);
    if (mongoInfo?.inherit) {
      return true;
    }
    if (mongoInfo?.from) {
      const fromNode = schema._typeMap[mongoInfo?.from]?.astNode;
      if (fromNode) {
        return isInheritDescendant(fromNode);
      }
    }
    if (node.interfaces) {
      const fromNode =
        schema._typeMap?.[node.interfaces[0]?.name?.value]?.astNode;
      if (fromNode) {
        return isInheritDescendant(fromNode);
      }
    }
    if (node._interfaces) {
      const fromNode = node._interfaces[0]?.astNode;
      if (fromNode) {
        return isInheritDescendant(fromNode);
      }
    }

    return false;
  };

  const isAbstractDescendant = (node) => {
    if (isInheritDescendant(node)) {
      return false;
    }

    const mongoInfo = getDirective("mongo", node);
    if (mongoInfo?.abstract) {
      return true;
    }
    if (mongoInfo?.from) {
      const fromNode = schema._typeMap[mongoInfo?.from]?.astNode;
      if (fromNode) {
        return isAbstractDescendant(fromNode);
      }
    }
    if (node.interfaces) {
      const fromNode =
        schema._typeMap?.[node.interfaces[0]?.name?.value]?.astNode;
      if (fromNode) {
        return isAbstractDescendant(fromNode);
      }
    }
    if (node._interfaces) {
      const fromNode = node._interfaces[0]?.astNode;
      if (fromNode) {
        return isAbstractDescendant(fromNode);
      }
    }
    return false;
  };

  const printedSchema = printSchemaWithDirectives(schema); // Returns a string representation of the schema
  const cleanPrintedSchema = printSchema(schema);
  console.log(cleanPrintedSchema);
  const astNode = parse(printedSchema); // Transforms the string into ASTNode
  const visitor = {
    InterfaceTypeDefinition: (node) => {
      const mongoInfo = getDirective("mongo", node);

      if (mongoInfo) {
        let { abstract, inherit, collection } = mongoInfo;
        let modelName = collection || singularize(underscore(node.name.value));
        const fields = getFields(node);
        if (!(abstract && inherit) && isInheritDescendant(node)) {
          inherited[node.name.value] = {
            ...mongoInfo,
            inherits: true,
            fields,
            collection: modelName,
          };
        } else if (abstract) {
          abstracts[node.name.value] = { ...mongoInfo, fields };
        } else if (inherit) {
          inherited[node.name.value] = {
            ...mongoInfo,
            fields,
            collection: modelName,
          };
        }
      }
    },
    ObjectTypeDefinition: (node) => {
      const mongoInfo = getDirective("mongo", node);
      const { collection, model, embedded } = mongoInfo || {};
      let modelName = collection || node.name.value;
      const fields = getFields(node);
      let modelData = null;

      if (isInheritDescendant(node)) {
        const from =
          schema._typeMap?.[node.interfaces[0]?.name?.value]?.astNode;
        if (from) {
          modelData = {
            fields,
            inherits: true,
            from: from.name.value,
            ...mongoInfo,
          };
        }
      } else if (isAbstractDescendant(node)) {
        const from =
          schema._typeMap?.[node.interfaces[0]?.name?.value]?.astNode;
        if (from) {
          modelData = {
            collection: inflected.underscore(modelName),
            fields,
            from: from.name.value,
            ...mongoInfo,
          };
        }
      } else if (model) {
        modelData = {
          collection: inflected.underscore(modelName),
          fields,
          from: from.name.value,
          ...mongoInfo,
        };
      } else if (embedded) {
        modelData = {
          fields,
          ...mongoInfo,
        };
      }

      if (modelData) {
        if (!embedded) {
          models[modelName] = modelData;
        } else {
          embeddedModels[modelName] = modelData;
        }
      }
    },
  };

  visit(astNode, { leave: visitor });
  return {
    models,
    embeddedDocuments: embeddedModels,
    abstracts,
    inherited,
    schema,
  };
};
