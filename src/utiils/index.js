import fs from "fs";
import path from "path";
import shell from "shelljs";
import prettier from "prettier";
import { defaultFieldResolver } from "graphql";

const Scalars = {
  Int: "Number",
  Date: "Date",
  Boolean: "Boolean",
  String: "String",
  Float: "Decimal",
  ObjectID: "ObjectId",
};

export const ModifierMethods = {
  "": (fieldName) => `(value) => ({ ${fieldName} : value })`,
  all: (fieldName) => `(value) => ({ ${fieldName} : {$all: value} })`,
  not: (fieldName) => `(value) => ({ ${fieldName} : {$not: {$eq: value}} })`,
  size: (fieldName) => `(value) => ({ ${fieldName} : {$size: value} })`,
  not_size: (fieldName) =>
    `(value) => ({ ${fieldName} : {$not: {$size: value}} })`,
  exact: (fieldName) => `(value) => ({ ${fieldName} : value })`,
  in: (fieldName) => `(value) => ({ ${fieldName} : {$in: value} })`,
  not_in: (fieldName) => `(value) => ({ ${fieldName} : {$nin: value} })`,
  lt: (fieldName) => `(value) => ({ ${fieldName} : {$lt: value} })`,
  gt: (fieldName) => `(value) => ({ ${fieldName} : {$gt: value} })`,
  lte: (fieldName) => `(value) => ({ ${fieldName} : {$lte: value} })`,
  gte: (fieldName) => `(value) => ({ ${fieldName} : {$gte: value} })`,
  exists: (fieldName) => `(value) => ({ ${fieldName} : {$exists: value} })`,
  contains: (fieldName) => `(value) => ({ ${fieldName} : new RegExp(value) })`,
  icontains: (fieldName) =>
    `(value) => ({ ${fieldName} : new RegExp(value, "i") })`,
  not_contains: (fieldName) =>
    `(value) => ({ ${fieldName} : {$not: new RegExp(value)} })`,
  not_icontains: (fieldName) =>
    `(value) => ({ ${fieldName} : {$not: new RegExp(value, "i")} })`,
  starts_with: (fieldName) =>
    `(value) => ({ ${fieldName} : new RegExp(\`^\${value}\`) })`,
  istarts_with: (fieldName) =>
    `(value) => ({ ${fieldName} : new RegExp(\`^\${value}\`, "i") })`,
  not_starts_with: (fieldName) =>
    `(value) => ({ ${fieldName} : {$not: new RegExp(\`^\${value}\`) }})`,
  not_istarts_with: (fieldName) =>
    `(value) => ({ ${fieldName} : {$not: new RegExp(\`^\${value}\`, "i")} })`,
  ends_with: (fieldName) =>
    `(value) => ({ ${fieldName} : new RegExp(\`\${value}$\`) })`,
  iends_with: (fieldName) =>
    `(value) => ({ ${fieldName} : new RegExp(\`\${value}$\`, "i") })`,
  not_ends_with: (fieldName) =>
    `(value) => ({ ${fieldName} : {$not: new RegExp(\`\${value}$\`)} })`,
  not_iends_with: (fieldName) =>
    `(value) => ({ ${fieldName} : {$not: new RegExp(\`\${value}$\`, "i")} })`,
};

export const Modifiers = {
  Boolean: ["", "not", "exists", "exact"],
  ID: ["", "in", "not_in", "exists"],
  ObjectID: ["", "in", "not", "not_in", "exists"],
  Int: ["", "in", "not", "not_in", "lt", "lte", "gt", "gte", "exists"],
  Float: ["", "in", "not", "not_in", "lt", "lte", "gt", "gte", "exists"],
  Date: ["", "not", "lt", "lte", "gt", "gte", "exists"],
  Many: ["all", "exact", "in", "not_in", "size", "not_size"],
  BooleanModifiers: ["exists"],
  IntModifiers: ["size", "not_size"],
  RelationshipModifiers: ["exists", "not", "in", "not_in"],
  String: [
    "",
    "in",
    "not_in",
    "contains",
    "icontains",
    "not_contains",
    "not_icontains",
    "starts_with",
    "istarts_with",
    "not_starts_with",
    "not_istarts_with",
    "ends_with",
    "iends_with",
    "not_ends_with",
    "not_iends_with",
    "not",
    "exists",
  ],
};

export const getFieldType = (type) => {
  let many = false;
  let noNull = false;
  if (type.indexOf("[") === 0) {
    many = true;
    type = type.replace("[", "").replace("]", "");
  }
  if (type.includes("!")) {
    noNull = true;
    type = type.replace(/!/gi, "");
  }
  return { type, nonNull: noNull, many };
};

export const getInterfaceFields = (from, typeMap) => {
  let fields = {};
  let iface = typeMap.abstracts[from] || typeMap.inherited[from];

  fields = { ...fields, ...iface.fields };
  const { from: f } = iface;
  if (f) {
    fields = { ...getInterfaceFields(f, typeMap), ...fields };
  }
  return fields;
};

export const getMongooseSchemaTemplateFields = (fields, typeMap) => {
  let imports = new Set();
  const _fields = Object.entries(fields)
    .map(([fieldName, field]) => {
      const { db_name, relation } = field;
      const fieldType = getFieldType(field.type);
      let type = Scalars[fieldType.type];
      const alias = fieldName !== db_name ? `,alias: "${fieldName}"` : "";
      let isModel = false;

      if (!type) {
        if (relation === "relation" && typeMap.models[fieldType.type]) {
          type = `mongoose.Schema.Types.${Scalars["ObjectID"]}`;
          isModel = true;
        } else if (typeMap.embeddedDocuments[fieldType.type]) {
          // imports.add(
          //   `import {${fieldType.type}Schema} from "../embeddedDocuments/${fieldType.type}"`
          // );
          //
          // type = `${fieldType.type}Schema`;
        }
      } else {
        type = `mongoose.Schema.Types.${type}`;
      }
      const ref = isModel ? `, ref: "${fieldType.type}"` : "";
      if (type) {
        let typeString = `{type:${type}${alias}${ref}}`;
        if (fieldType.many) {
          typeString = `[${typeString}]`;
        }
        return `\t${field.db_name} : ${typeString}`;
      }
      return null;
    })
    .filter((f) => !!f);
  return [_fields, imports];
};

export const TemplateObject = (response, supportFiles = []) => {
  return {
    toString: () => {
      return response;
    },
    toFile: (_path, parser = "babel") => {
      if (!response) {
        return;
      }
      const fileDirPath = path.dirname(_path);
      if (!fs.existsSync(fileDirPath)) {
        shell.mkdir("-p", fileDirPath);
      }
      fs.writeFileSync(_path, prettier.format(response, { parser }), "utf8");

      supportFiles.forEach(({ response, path: _p, parser: _parser }) => {
        TemplateObject(response).toFile(path.join(fileDirPath, _p), _parser);
      });
    },
  };
};

export const dateResolver = defaultFieldResolver;

export const getImplementations = (typeName, info, typeMap) => {
  let fromNode;
  let from;
  if (Object.keys(typeMap.models).includes(typeName)) {
    if (info?.inherits) {
      fromNode = typeMap.inherited[info.from];
      from = info.from;
    } else if (info?.from) {
      fromNode = typeMap.abstracts[info.from];
      from = info.from;
    }
  } else {
    if (info.from) {
      fromNode = typeMap.inherited[info.from] || typeMap.abstracts[info.from];
      from = info.from;
    }
  }
  if (fromNode) {
    return [from, ...getImplementations(from, fromNode, typeMap)];
  }
  return [];
};

export const buildExtendedFields = (typeName, info, typeMap) => {
  const fieldList = [];
  let fields = {};
  const { from, inherits } = info;

  if (from || inherits) {
    fields = getInterfaceFields(from || inherits, typeMap);
  }

  fields = Object.entries({ ...fields, ...info.fields });
  for (let i = 0; i < fields.length; i++) {
    const [fieldName, field] = fields[i];
    let fieldArgs = "";
    const { type } = getFieldType(field.type);
    if (type === "Date") {
      fieldArgs = `(format:String, relative:Boolean=false)`;
    }
    fieldList.push([fieldName, field.type, fieldArgs]);
  }
  return fieldList;
};

export const buildWhereList = (typeName, info, typeMap) => {
  const fieldList = [];
  let fields = {};
  const { from, inherits } = info;

  if (from || inherits) {
    fields = getInterfaceFields(from || inherits, typeMap);
  }

  fields = Object.entries({ ...fields, ...info.fields });
  const _imports = new Set();
  for (let i = 0; i < fields.length; i++) {
    const [fieldName, field] = fields[i];
    const { relation } = field;
    const { type, nonNull, many } = getFieldType(field.type);
    if (relation) {
      const clsName = `${type}SubQuery`;
      _imports.add(
        `import {${clsName}} from "../../../${type}/js/${type}RelationshipResolvers";`
      );
      const inputType = `${type}WhereInput`;
      fieldList.push([
        fieldName,
        {
          inputResolver: `${clsName}({fieldName:"${field.db_name}"})`,
          inputType,
        },
      ]);

      let modifiers = Modifiers.RelationshipModifiers;
      if (many) {
        modifiers = Array.from(new Set([...modifiers, ...Modifiers.Many]));
      }

      modifiers.forEach((modifier) => {
        const mapName = modifier ? `${fieldName}_${modifier}` : fieldName;
        const method = ModifierMethods[modifier ?? ""];
        let inputResolver;
        if (method) {
          inputResolver = method(field.db_name);
        }
        let inputType = type;

        if (Modifiers.BooleanModifiers.includes(modifier)) {
          inputType = "Boolean";
        } else if (Modifiers.IntModifiers.includes(modifier)) {
          inputType = "Int";
        } else {
          inputType = "ObjectID";
        }
        if (Modifiers.Many.includes(modifier)) {
          inputType = `[${inputType}]`;
        }
        if (inputType) {
          fieldList.push([mapName, { inputResolver, inputType }]);
        }
      });
    } else if (typeMap.embeddedDocuments[type]) {
    } else {
      let modifiers = Modifiers[type] || [];
      if (many) {
        modifiers = Array.from(new Set([...modifiers, ...Modifiers.Many]));
      }
      modifiers.forEach((modifier) => {
        const mapName = modifier ? `${fieldName}_${modifier}` : fieldName;
        const method = ModifierMethods[modifier ?? ""];
        let inputResolver;
        if (method) {
          inputResolver = method(field.db_name);
        }
        let inputType = type;

        if (Modifiers.BooleanModifiers.includes(modifier)) {
          inputType = "Boolean";
        } else if (Modifiers.IntModifiers.includes(modifier)) {
          inputType = "Int";
        } else if (Modifiers.Many.includes(modifier) && modifier !== "exact") {
          inputType = `[${type}]`;
        }
        fieldList.push([mapName, { inputResolver, inputType }]);
      });
    }
  }
  return [fieldList, _imports];
};

export const buildCreateList = (typeName, info, typeMap) => {
  const fieldList = [];
  let fields = {};
  const { from, inherits } = info;

  if (from || inherits) {
    fields = getInterfaceFields(from || inherits, typeMap);
  }

  fields = Object.entries({ ...fields, ...info.fields });
  const _imports = new Set([]);
  for (let i = 0; i < fields.length; i++) {
    const [fieldName, field] = fields[i];

    let { type, nonNull, many } = getFieldType(field.type);
    const { relation } = field;
    if (fieldName === "id") {
      nonNull = false;
    }
    if (relation) {
      const clsName = `${type}Create${many ? "Many" : ""}RelationResolver`;
      _imports.add(
        `import {${clsName}} from "../../../${type}/js/${type}RelationshipResolvers";`
      );
      fieldList.push([
        fieldName,
        {
          inputType: `${type}Create${many ? "Many" : "One"}RelationInput`,
          inputResolver: `${clsName}({fieldName:"${field.db_name}", many:${
            many ? "true" : "false"
          }})`,
        },
      ]);
    } else if (typeMap.embeddedDocuments[type]) {
      // if (many) {
      //   fieldList.push([fieldName, { inputType: `[${type}Input!]` }]);
      // } else {
      //   fieldList.push([fieldName, { inputType: `${type}Input` }]);
      // }
    } else {
      let inputType = `${type}${nonNull ? "!" : ""}`;
      if (many) {
        inputType = `[${inputType}]`;
      }
      fieldList.push([
        fieldName,
        {
          inputResolver: `(record, value, context, info) => ({ ${field.db_name} : value })`,
          inputType,
        },
      ]);
    }
  }
  return [fieldList, _imports];
};

export const buildUpdateList = (typeName, info, typeMap) => {
  const fieldList = [];
  let fields = {};
  const { from, inherits } = info;

  if (from || inherits) {
    fields = getInterfaceFields(from || inherits, typeMap);
  }

  fields = Object.entries({ ...fields, ...info.fields });
  const _imports = new Set([]);
  for (let i = 0; i < fields.length; i++) {
    const [fieldName, field] = fields[i];
    if (fieldName === "id") {
      continue;
    }
    const { type, nonNull, many } = getFieldType(field.type);
    if (field.relation) {
      const clsName = `${type}Update${many ? "Many" : ""}RelationResolver`;
      _imports.add(
        `import {${clsName}} from "../../../${type}/js/${type}RelationshipResolvers";`
      );
      fieldList.push([
        fieldName,
        {
          inputType: `${type}Update${many ? "Many" : "One"}RelationInput`,
          inputResolver: `${clsName}({fieldName:"${field.db_name}", many: ${
            many ? "true" : "false"
          }})`,
        },
      ]);
    } else if (typeMap.embeddedDocuments[type]) {
      // if (many) {
      //   fieldList.push([fieldName, { inputType: `[${type}Input!]` }]);
      // } else {
      //   fieldList.push([fieldName, { inputType: `${type}Input` }]);
      // }
    } else {
      let inputType = `${type}${nonNull ? "!" : ""}`;
      if (many) {
        inputType = `[${inputType}]`;
      }
      fieldList.push([
        fieldName,
        {
          inputType,
          inputResolver: `(record, value, context, info) => ({${field.db_name} : value })`,
        },
      ]);
    }
  }
  return [fieldList, _imports];
};

export const buildSortList = (typeName, info, typeMap) => {
  const sortNames = [];
  let fields = {};
  const { from, inherits } = info;

  if (from || inherits) {
    fields = getInterfaceFields(from || inherits, typeMap);
  }

  fields = Object.entries({ ...fields, ...info.fields });
  for (let i = 0; i < fields.length; i++) {
    const [fieldName, field] = fields[i];
    sortNames.push([`${fieldName}_ASC`, { [field.db_name]: 1 }]);
    sortNames.push([`${fieldName}_DESC`, { [field.db_name]: -1 }]);
  }
  return sortNames;
};
