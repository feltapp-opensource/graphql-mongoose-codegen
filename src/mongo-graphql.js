import path from "path";
import { GraphQLOperations } from "./templates/graphql-server/GraphQLOperations";
import { GraphQLOperationResolvers } from "./templates/graphql-server/GraphQLOperationResolvers";
import {
  GraphQLEmbeddedOperationTypes,
  GraphQLOperationTypes,
} from "./templates/graphql-server/GraphQLOperationTypes";
import { GraphQLFieldResolvers } from "./templates/graphql-server/GraphQLResolvers";
import { extractData } from "./utiils/visitor";
import { MongooseInterfaceSchemaTemplate } from "./templates/mongoose/MongooseAbstract";
import { GraphQLTypeExtension } from "./templates/graphql-server/GraphQLTypeExtension";
import { MongooseSchemaTemplate } from "./templates/mongoose/MongooseSchema";
import { TemplateObject } from "./utiils";
import { GraphQLServerResolvers } from "./templates/graphql-server/GraphQLServerResolvers";
import { GraphQLServer } from "./templates/graphql-server/GraphQLServer";
import { MongooseEmbeddedSchemaTemplate } from "./templates/mongoose/MongooseEmbeddedSchema";
import { MongooseInheritedSchemaTemplate } from "./templates/mongoose/MongooseInheritedSchema";

export const mongoGraphql = {
  plugin: (schema, documents, config) => {
    const { embeddedDocuments, models, abstracts, inherited } = extractData(
      schema
    );
    const typeMap = {
      embeddedDocuments,
      models,
      abstracts,
      inherited,
      schema,
    };
    Object.entries({ ...abstracts }).forEach(([typeName, info]) => {
      MongooseInterfaceSchemaTemplate(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "mongo",
          "interfaces",
          `${typeName}.js`
        )
      );
    });
    Object.entries({ ...inherited }).forEach(([typeName, info]) => {
      MongooseInheritedSchemaTemplate(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "mongo",
          "models",
          `${typeName}.js`
        )
      );
      GraphQLOperations(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          typeName,
          `${typeName}Operations.graphql`
        ),
        "graphql"
      );
      GraphQLOperationResolvers(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          typeName,
          "js",
          `${typeName}OperationResolvers.js`
        )
      );
      GraphQLTypeExtension(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          typeName,
          `${typeName}Extension.graphql`
        ),
        "graphql"
      );
      GraphQLOperationTypes(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          typeName,
          `${typeName}OperationTypes.graphql`
        ),
        "graphql"
      );
      GraphQLFieldResolvers(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          typeName,
          "js",
          `${typeName}FieldResolvers.js`
        )
      );
    });

    Object.entries(embeddedDocuments).forEach(([typeName, info]) => {
      MongooseEmbeddedSchemaTemplate(typeName, info, {
        models,
        embeddedDocuments,
      }).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "mongo",
          "embeddedDocuments",
          `${typeName}.js`
        )
      );
      GraphQLTypeExtension(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          "embeddedDocuments",
          typeName,
          `${typeName}Extension.graphql`
        ),
        "graphql"
      );
      GraphQLEmbeddedOperationTypes(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          "embeddedDocuments",
          typeName,
          `${typeName}InputTypes.graphql`
        ),
        "graphql"
      );
      GraphQLFieldResolvers(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          "embeddedDocuments",
          typeName,
          "js",
          `${typeName}FieldResolvers.js`
        )
      );
    });

    Object.entries(models).forEach(([typeName, info]) => {
      MongooseSchemaTemplate(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "mongo",
          "models",
          `${typeName}.js`
        )
      );
      GraphQLOperations(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          typeName,
          `${typeName}Operations.graphql`
        ),
        "graphql"
      );
      GraphQLOperationResolvers(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          typeName,
          "js",
          `${typeName}OperationResolvers.js`
        )
      );
      GraphQLTypeExtension(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          typeName,
          `${typeName}Extension.graphql`
        ),
        "graphql"
      );
      GraphQLOperationTypes(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          typeName,
          `${typeName}OperationTypes.graphql`
        ),
        "graphql"
      );
      GraphQLFieldResolvers(typeName, info, typeMap).toFile(
        path.join(
          process.cwd(),
          "src",
          "generated",
          "graphql",
          typeName,
          "js",
          `${typeName}FieldResolvers.js`
        )
      );
      // ApolloClientSchema(typeName, info, {
      //   models,
      //   embeddedDocuments,
      // }).toFile(
      //   path.join(
      //     process.cwd(),
      //     "src",
      //     "generated",
      //     "apollo",
      //     "schema",
      //     `${typeName}.js`
      //   )
      // );
    });
    GraphQLServerResolvers(models, embeddedDocuments, inherited).toFile(
      path.join(process.cwd(), "src", "generated", "graphql", "resolvers.js")
    );
    GraphQLServer().toFile(
      path.join(process.cwd(), "src", "generated", "graphql", "server.js")
    );
    return "";
  },
};
