import { codegen } from "@graphql-codegen/core";
import { mongoGraphql } from "./mongo-graphql";
import { loadSchemaSync } from "@graphql-tools/load";
import { GraphQLFileLoader } from "@graphql-tools/graphql-file-loader";

const runCodeGen = async () => {
  const schema5 = loadSchemaSync(["src/test/**/*.graphql"], {
    // load from multiple files using glob
    assumeValid: true,
    assumeValidSDL: true,
    loaders: [new GraphQLFileLoader()],
  });
  const config = {
    // used by a mongoGraphql internally, although the 'typescript' mongoGraphql currently
    // returns the string output, rather than writing to a file
    filename: "output.ts",
    schema: schema5,
    plugins: [
      // Each mongoGraphql should be an object
      {
        mongoGraphql: {
          namingConvention: {
            collection: "snake",
            type: "pascal",
            field: "underscore",
          },
        }, // Here you can pass configuration to the mongoGraphql
      },
    ],
    pluginMap: {
      mongoGraphql: mongoGraphql,
    },
  };
  const output = await codegen(config);
  console.log(output);
};

runCodeGen().then(() => {});
