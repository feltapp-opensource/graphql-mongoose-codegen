import path from "path";
import GraphQLServer from "./generated/graphql/server";
GraphQLServer({
  schemaPaths: [
    path.join(process.cwd(), "src/initial.graphql"),
    path.join(process.cwd(), "src/test/**/*.graphql"),
  ],
  mongoUrl: process.env.MONGO_URL,
  dbName: 'tester',
  port: process.env.PORT || 4000,
});
