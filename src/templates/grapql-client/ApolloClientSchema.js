import { TemplateObject } from "../../utiils";
import * as inflection from "inflected";

export const ApolloClientSchema = (typeName, info, typeMap) => {
  const singular = inflection.camelize(typeName, false);
  const plural = inflection.pluralize(inflection.camelize(typeName, false));
  const pluralType = inflection.pluralize(inflection.camelize(typeName, true));
  const response = `import {gql} from "apollo-boost";
import GraphqlClient from "../client";
import {useQuery} from "@apollo/react-hooks";
import {useGraphQLRecord} from "../hooks/useGraphQLRecord";
    
const ${typeName} = {
  queries: {},
  mutations: {},
  fragments: {},
  operations: {},
  parsers: {},
  hooks: {}
};

${typeName}.fragments.all${typeName}Fields = gql\`
  fragment all${typeName}Fields on ${typeName} {
    id
    createdAt
    updatedAt
  }
\`

${typeName}.queries.fetchAllPaged = gql\`
  \${${typeName}.fragments.all${typeName}Fields}
  query fetchAll${pluralType}Paged(
    $where: ${typeName}WhereInput
    $first: Int
    $skip: Int
    $sort: [${typeName}OrderByInput!]
    $orderBy: ${typeName}OrderByInput
  ) {
    paged: ${plural}Paged(where: $where, first: $first, skip: $skip, sort: $sort, orderBy:$orderBy) {
      total
      ${plural} {
        ...all${typeName}Fields
      }
    }
  }
\`;

${typeName}.queries.byId = gql\`
  \${${typeName}.fragments.all${typeName}Fields}
  query fetch${typeName}ById($id: ObjectID!){
    ${singular}(where:{id:$id}) {
       ...all${typeName}Fields
    }
  }
\`

${typeName}.queries.fetchAll = gql\`
  \${${typeName}.fragments.all${typeName}Fields}
  query fetchAll${pluralType}ById($where: ${typeName}WhereInput, $sort:[${typeName}OrderByInput], $orderBy:${typeName}OrderByInput, $skip:Int=0, $first:Int){
    ${plural}(where:$where, sort:$sort, orderBy:$orderBy, skip:$skip, first:$first) {
       ...all${typeName}Fields
    }
  }
\`

${typeName}.mutations.create = gql\`
  \${${typeName}.fragments.all${typeName}Fields}
  mutation create${typeName}($data: ${typeName}CreateInput!) {
    ${singular}: create${typeName}(data: $data) {
      ...all${typeName}Fields
    }
  }
\`;


${typeName}.mutations.updateOne = gql\`
  \${${typeName}.fragments.all${typeName}Fields}
  mutation updateOne${typeName}($id: ObjectID!, $data: ${typeName}UpdateInput!) {
    ${singular}: update${typeName}(where: { id: $id }, data: $data) {
      ...all${typeName}Fields
    }
  }
\`;

${typeName}.mutations.deleteOne = gql\`
  mutation deleteOne${typeName}($id: ObjectID) {
    ${singular}: delete${typeName}(where: { id: $id }) {
      id
    }
  }
\`;

${typeName}.operations.byId = (id, options = {}) => {
  const client = GraphqlClient.client;
  return client
    .query({
      query: ${typeName}.queries.byId,
      variables: { id },
      ...options
    })
    .then(({ data: { ${singular} } }) => ${singular});
};


${typeName}.operations.fetchAll = (options = {}) => {
  const client = GraphqlClient.client;
  return client
    .query({
      query: ${typeName}.queries.fetchAll,
      ...options
    })
    .then(({ data: { ${singular} } }) => ${singular});
};

${typeName}.operations.fetchAllPaged = (where, options = {}) => {
  const client = GraphqlClient.client;
  return client
    .query({
      query: ${typeName}.queries.fetchAll,
      ...options
    })
    .then(({ data: { total, ${plural} } }) => ({ total, ${plural} }));
};

${typeName}.operations.create = async (data, options = {}) => {
  const client = GraphqlClient.client;
  return client
    .mutate({
      mutation: ${typeName}.mutations.create,
      variables: {
        data
      },
      ...options
    })
    .then(({ data: { ${singular} } }) => {
      return ${singular};
    });
};

${typeName}.operations.deleteOne = (id, options = {}) => {
  console.log(id);
  const client = GraphqlClient.client;
  
  return client
    .mutate({
      mutation: ${typeName}.mutations.deleteOne,
      variables: { id },
      ...options
    })
    .then(() => true);
};


${typeName}.operations.updateOne = async (id, data, options = {}) => {
  const client = GraphqlClient.client;
  return client
    .mutate({
      mutation: ${typeName}.mutations.updateOne,
      variables: {
        id,
        data
      },
      ...options
    })
    .then(({ data: { ${singular} } }) => {
      return ${singular};
    });
};


${typeName}.hooks.use${pluralType}PagedQuery = ({ where, sort, first, skip }, options = {}) => {
  const { data, loading, error } = useQuery(${typeName}.queries.fetchAllPaged, {
    ...options,
    variables: { where, sort, first, skip }
  });

  return useMemo(() => {
    return { ${plural}: data?.paged?.${plural}, total: data?.paged?.total, loading, error };
  }, [data, loading, error]);
};

${typeName}.hooks.use${pluralType}Query = ({ where }, options = {}) => {
  const { data, loading, error } = useQuery(${typeName}.queries.fetchAll, {
    ...options,
    variables: { where }
  });

  return { ${plural}: data?.${plural}, loading, error };
};

${typeName}.hooks.use${typeName}ByIdQuery = (id, options = {}) => {
  const { data, loading, error, refetch } = useQuery(
    ${typeName}.queries.byId,
    {
      ...options,
      skip: !id,
      variables: { id }
    }
  );
  
  return { ${singular}: data?.${singular}, loading, error };
};

${typeName}.hooks.use${typeName}Record = id => {
  let options = {
    awaitRefetchQueries: true
  };
  let { record, ...recordProps } = useGraphQLRecord({
    record_id: id,
    relationshipInfo: {},
    query: ${typeName}.operations.byId,
    mutate: ({ id, data, delete: del }) => {
      if (id) {
        if (del) {
          return ${typeName}.operations.deleteOne(id, options);
        }
        return ${typeName}.operations.updateOne(id, data, options);
      } else if (data) {
        return ${typeName}.operations.create(data, options);
      }
      return Promise.reject("Invalid save params");
    }
  });
  return { record, ...recordProps };
};

`;
  return TemplateObject(response);
};
