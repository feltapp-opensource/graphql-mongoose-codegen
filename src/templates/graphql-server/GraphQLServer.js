import { TemplateObject } from "../../utiils";

export const GraphQLServer = () => {
  const response = `import mongoose from "mongoose";
import path from "path";
import { loadSchemaSync } from "@graphql-tools/load";
import { GraphQLFileLoader } from "@graphql-tools/graphql-file-loader";
import { addResolversToSchema } from "@graphql-tools/schema";
import { ApolloServer } from "apollo-server";
import * as _ from "lodash";

import MongoResolvers from "./resolvers";

const mongoGraphqlServer = ({
  resolvers,
  mongoUrl,
  dbName,
  port = 4000,
  host = "0.0.0.0",
  generatedPath = "src/generated/",
  schemaPaths = [],
  ...apolloServerArgs
}) => {
  const initialSchema = loadSchemaSync(
    [
      ...schemaPaths,
      path.join(process.cwd(), generatedPath, "graphql/**/*.graphql"),
    ],
    {
      // load from multiple files using glob
      assumeValid: true,
      assumeValidSDL: true,
      loaders: [new GraphQLFileLoader()],
    }
  );
  const schema = addResolversToSchema({
    schema: initialSchema,
    resolvers: _.merge({}, MongoResolvers, resolvers),
  });

  const server = new ApolloServer({ schema, ...apolloServerArgs });
  mongoose
    .connect(mongoUrl, {
      dbName: dbName,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then((db) => {
      return server.listen({ port, host });
    })
    .then(({ url }) => {
      console.log(\`🚀  Server ready at \${url}\`);
    });
};
export default mongoGraphqlServer;

`;
  return TemplateObject(response);
};
