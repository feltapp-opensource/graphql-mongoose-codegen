import { TemplateObject } from "../../utiils";

export const GraphQLServerResolvers = (
  models,
  embeddedDocuments,
  inherited
) => {
  const resolverImports = new Set();
  const resolverExports = new Set();
  Object.entries({...models, ...inherited}).forEach(([typeName, info]) => {
    resolverImports.add(
      `import ${typeName}Resolvers from "./${typeName}/js/${typeName}Resolvers";`
    );
    resolverExports.add(`...${typeName}Resolvers`);
  });
  // Object.entries(embeddedDocuments).forEach(([typeName, info]) => {
  //   resolverImports.add(
  //     `import ${typeName}Resolvers from "./embeddedDocuments/${typeName}/js/${typeName}Resolvers";`
  //   );
  //   resolverExports.add(`...${typeName}Resolvers`);
  // });

  const response = `import * as _ from "lodash"
${Array.from(resolverImports).join("\n")}
  
export default _.merge({},${Array.from(resolverExports).join(",")})
`;

  return TemplateObject(response);
};
