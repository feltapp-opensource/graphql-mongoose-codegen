import { TemplateObject } from "../../utiils";
import * as inflection from "inflected";
export const GraphQLOperations = (typeName, info, typeMap) => {
  const single = inflection.camelize(typeName, false);
  const plural = inflection.pluralize(inflection.camelize(typeName, false));

  let mutation = `extend type Mutation {
  create${typeName}(data: ${typeName}CreateInput) : ${typeName}
  update${typeName}(where:${typeName}WhereUniqueInput!, data:${typeName}UpdateInput!) : ${typeName}
  delete${typeName}(where:${typeName}WhereUniqueInput!) : ${typeName}
}`;

  if (Object.keys(typeMap.inherited).includes(typeName)) {
    mutation = `extend type Mutation {
  create${typeName}(data: ${typeName}CreateInput) : ${typeName}
  update${typeName}(where:${typeName}WhereUniqueInput!, data:${typeName}UpdateInput!) : ${typeName}
  delete${typeName}(where:${typeName}WhereUniqueInput!) : ${typeName}
}`;
  }

  const response = `extend type Query {
  ${single}(where:${typeName}WhereUniqueInput, sort:[${typeName}OrderByInput!], skip: Int = 0) : ${typeName}
  ${plural}(where:${typeName}WhereInput, sort:[${typeName}OrderByInput!], skip: Int = 0, first:Int, orderBy:${typeName}OrderByInput) : [${typeName}]
  ${plural}Paged(where:${typeName}WhereInput, sort:[${typeName}OrderByInput!], skip: Int = 0, first:Int, orderBy:${typeName}OrderByInput) : ${typeName}Pagination!
  ${single}Aggregation(pipeline:JSON!) : JSON
}

${mutation}
`;
  return TemplateObject(response);
};
