import { getFieldType, getInterfaceFields, TemplateObject } from "../../utiils";
import { singularize, underscore } from "inflected";

export const GraphQLFieldResolvers = (typeName, info, typeMap) => {
  const resolverList = [];
  let fields = info?.inherit
    ? { __resolveType: `obj=>obj._cls.split('.').pop(),` }
    : {};
  const { from, inherits } = info;

  if (from || inherits) {
    fields = { ...fields, ...getInterfaceFields(from || inherits, typeMap) };
  }

  fields = Object.entries({ ...fields, ...info.fields });
  const imports = new Set([]);
  for (let i = 0; i < fields.length; i++) {
    const [fieldName, field] = fields[i];
    if (fieldName === "__resolveType") {
      resolverList.push(`${fieldName} : ${field}`);
    } else {
      const { relation } = field;
      let { type, nonNull, many } = getFieldType(field.type);
      if (relation === "relation") {
        const resolver = `${type}${many ? "Many" : ""}RelationResolver`;
        imports.add(
          `import {${resolver}} from "../../${type}/js/${type}RelationshipResolvers";`
        );
        resolverList.push(
          `${fieldName} : ${resolver}({dbName:"${field.db_name}",_cls:${typeMap.models[type]?._cls}}),`
        );
      } else if (relation === "extRelation") {
        const resolver = `${type}${many ? "Many" : ""}ExternalRelationResolver`;
        imports.add(
          `import {${resolver}} from "../../${type}/js/${type}ExternalRelationshipResolvers";`
        );
        resolverList.push(
          `${fieldName} : ${resolver}({dbName:"${singularize(
            underscore(typeName)
          )}",_cls:${typeMap.models[type]?._cls}}),`
        );
      } else if (type === "Date") {
        imports.add(`import {dateResolver} from "../../../../utiils"`);
        resolverList.push(`${fieldName} : dateResolver,`);
      } else {
        resolverList.push(`${fieldName} : defaultFieldResolver,`);
      }
    }
  }

  const response = `import {defaultFieldResolver} from "graphql";
  ${Array.from(imports).join("\n")}
  
export default {
${typeName}: {
  ${resolverList.join("\n")}
  }    
}
`;
  return TemplateObject(response);
};
