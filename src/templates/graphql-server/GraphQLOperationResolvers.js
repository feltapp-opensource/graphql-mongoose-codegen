import {
  buildCreateList,
  buildSortList,
  buildUpdateList,
  buildWhereList,
  TemplateObject,
} from "../../utiils";
import * as inflection from "inflected";

export const GraphQLOperationResolvers = (typeName, info, typeMap) => {
  const single = inflection.camelize(typeName, false);
  const plural = inflection.pluralize(inflection.camelize(typeName, false));
  const sortMap = buildSortList(typeName, info, typeMap).reduce(
    (agg, [fieldName, resolve]) => {
      agg[fieldName] = resolve;
      return agg;
    },
    {}
  );

  const [whereFields, whereImports] = buildWhereList(typeName, info, typeMap);

  const whereMap = `{
  ${whereFields
    .reduce((agg, [fieldName, { inputResolver }]) => {
      agg.push(`${fieldName} : ${inputResolver},`);
      return agg;
    }, [])
    .join("\n")}
}`;

  const [createFields, createImports] = buildCreateList(
    typeName,
    info,
    typeMap
  );

  const [updateFields, updateImports] = buildUpdateList(
    typeName,
    info,
    typeMap
  );

  const createMap = `{
  ${createFields
    .reduce((agg, [fieldName, { inputResolver }]) => {
      agg.push(`${fieldName} : ${inputResolver},`);
      return agg;
    }, [])
    .join("\n")}
}`;

  const updateMap = `{
  ${updateFields
    .reduce((agg, [fieldName, { inputResolver }]) => {
      agg.push(`${fieldName} : ${inputResolver},`);
      return agg;
    }, [])
    .join("\n")}
}`;
  const resolversFile = `import ${typeName}FieldResolvers from "./${typeName}FieldResolvers";
import ${typeName}OperationResolvers from "./${typeName}OperationResolvers";
export default [${typeName}OperationResolvers, ${typeName}FieldResolvers];
`;
  const SortMapFile = `export default ${JSON.stringify(sortMap)}`;
  const WhereMapFile = `${Array.from(whereImports).join("\n")}
  
  export default ${whereMap}`;
  const CreateMapFile = `${Array.from(createImports).join("\n")}
  
  export default ${createMap}`;
  const UpdateMapFile = `${Array.from(updateImports).join("\n")}
  
  export default ${updateMap}`;
  const inputHelpers = `import * as _ from "lodash";
import {ObjectID} from "mongodb";
import { ${typeName} } from "../../../mongo/models/${typeName}";
import ${typeName}WhereMap from "./input-maps/where";
import ${typeName}SortMap from "./input-maps/sort";
import ${typeName}CreateMap from "./input-maps/create";
import ${typeName}UpdateMap from "./input-maps/update";

export const get${typeName}Where = async (where, context, info) => {
  const wheres = Object.entries(where);
  let selector = {};
  for(let i = 0; i < wheres.length; i++) {
    const [key, value] = wheres[i];
    selector = _.merge(selector, await ${typeName}WhereMap[key](value, context, info))
  }
  return selector;
};

export const get${typeName}Sorts = (sortOptions) => {
  return sortOptions.reduce((agg, option)=>{
    const val = ${typeName}SortMap[option];
    return {...agg, ...val};
  },{})
};
`;

  const externalRelationshipResolvers = `import {${typeName}} from "../../../mongo/models/${typeName}"
export const ${typeName}DataLoader = ${typeName}.makeDataLoader();

export const ${typeName}ExternalRelationResolver = ({ dbName, _cls}) => async (parent, args, context, info)=>{
  let options = {_cls};
  return ${typeName}DataLoader.byExternalId(dbName, parent[dbName], options);
};

export const ${typeName}ManyExternalRelationResolver = ({dbName, _cls}) => async (parent, args, context, info)=>{
  let options = {_cls};
  return ${typeName}DataLoader.byExternalId(dbName, parent[dbName], options, true);
  
};

export const create${typeName}ExternalRelationshipResolver = async ({
  fieldName,
  parent,
  create: data,
  many,
  allowDups,
  context,
  info,
}) => {
  const document = await ${typeName}.gqlCreate(data, {context,info}, {save:false});
  if (!many) {
    const oldDoc = await ${typeName}.findOne({[parent.collection.collectionName]:parent.id});
    if(oldDoc) {
      oldDoc.set(parent.collection.collectionName, undefined);
      await oldDoc.save()
    }
  }
  document.set(parent.collection.collectionName, parent.id)
  await document.save();
  return {}
};

export const update${typeName}ExternalRelationshipResolver = async ({
  fieldName,
  update,
  parent,
  context,
  info,
}) => {
  let record = await ${typeName}.findOne({ [parent.collection.collectionName]: parent.id });
  await record.gqlUpdate(update);
  return {};
};

export const connect${typeName}ExternalRelationshipResolver = async ({
  fieldName,
  parent,
  connect: { id },
  many,
  allowDups,
  context,
  info,
}) => {
  const record = await ${typeName}.findOne({ _id: id });
  if (!record) {
    throw new Error(\`Unable to connect ${typeName}:\${id} as it was not found.\`);
  }
  if (!many) {
    const oldDoc = await ${typeName}.findOne({[parent.collection.collectionName]:parent.id});
    if(oldDoc) {
      oldDoc.set(parent.collection.collectionName, undefined);
      await oldDoc.save()
    }
  }
  record.set(parent.collection.collectionName, record.id);
  record.save();
  return {}
};

export const disconnect${typeName}ExternalRelationshipResolver = async ({
  fieldName,
  disconnect,
  many,
  parent,
  allowDups,
  context,
  info,
}) => {
   const record = await ${typeName}.findOne({ _id: id });
  if (!record) {
    throw new Error(\`Unable to disconnect \${parent.collection.collectionName} as it was not found.\`);
  }
  record.set(parent.collection.collectionName, undefined);
  await record.save()
  return {}
};

export const delete${typeName}ExternalRelationshipResolver = async ({
  fieldName,
  delete: del,
  many,
  parent,
  allowDups,
  context,
  info,
}) => {
  const record = await ${typeName}.findOne({ _id: id });
  if (!record) {
    throw new Error(\`Unable to delete \${parent.collection.collectionName} as it was not found.\`);
  }
  await record.delete()
  return {}
};

export const ${typeName}CreateExternalRelationResolver = ({ fieldName, many }) => async (
  parent,
  { connect, create },
  context,
  info
) => {
  if (create) {
    await create${typeName}ExternalRelationshipResolver({
        fieldName,
        many,
        parent,
        create,
        context,
        info
      })
  }
  if(connect) {
    await connect${typeName}ExternalRelationshipResolver({
        fieldName,
        many,
        parent,
        connect,
        context,
        info
      });
  }
  return {};
};

export const ${typeName}CreateManyExternalRelationResolver = ({ fieldName }) => async (
  parent,
  { create: _create, connect: _connect },
  context,
  info
) => {
  const data = {};
  if (_connect) {
    for (let connect of _connect) {
      await ${typeName}CreateExternalRelationResolver({ fieldName, many: true })(
          parent,
          { connect },
          context,
          info
        )
    }
  }
  if (_create) {
    for (let create of _create) {
      await ${typeName}CreateExternalRelationResolver({ fieldName, many: true, update })(
          parent,
          { create },
          context,
          info
        )
    }
  }
  return data;
};

export const ${typeName}UpdateExternalRelationResolver = ({ fieldName, many }) => async (
  parent,
  { create, connect, update, delete: del, disconnect },
  context,
  info
) => {
  let data = {};

  if ((create || connect) && (del || disconnect)) {
    throw new Error(
      "Adding and removing documents at the same time is unsupported"
    );
  }
  if (create) {
    await create${typeName}ExternalRelationshipResolver({
        fieldName,
        many,
        parent,
        create,
        context,
        info
      })
  }
  if (connect) {
    await connect${typeName}ExternalRelationshipResolver({
        fieldName,
        many,
        parent,
        connect,
        context,
        info,
      })
  }
  if (update) {
    await update${typeName}ExternalRelationshipResolver({
        fieldName,
        many,
        parent,
        update,
        context,
        info,
      })
  }
  if (del) {
    await delete${typeName}ExternalRelationshipResolver({
        fieldName,
        many,
        parent,
        delete: del,
        context,
        info,
      })
  }

  if (disconnect) {
    await disconnect${typeName}ExternalRelationshipResolver({
        fieldName,
         many,
        parent,
        disconnect,
        context,
        info,
      })
  }
  return {};
};

export const ${typeName}UpdateManyExternalRelationResolver = ({ fieldName }) => async (
  parent,
  {
    create: _create,
    connect: _connect,
    delete: _del,
    disconnect: _disconnect,
    update: _update,
  },
  context,
  info
) => {
  if ((_create || _connect) && (_del || _disconnect)) {
    throw new Error(
      "Adding and removing documents at the same time is unsupported"
    );
  }

  if (_connect) {
    for (let connect of _connect) {
      await ${typeName}UpdateExternalRelationResolver({ fieldName, many: true })(
          parent,
          { connect },
          context,
          info
        )
    }
  }
  if (_create) {
    for (let create of _create) {
      await ${typeName}UpdateExternalRelationResolver({ fieldName, many: true })(
          parent,
          { create },
          context,
          info
        );
    }
  }
  if (_update) {
    for (let update of _update) {
      await ${typeName}UpdateExternalRelationResolver({ fieldName, many: true })(
          parent,
          { update },
          context,
          info
        );
    }
  }
  if (_del) {
    for (let del of _del) {
      await ${typeName}UpdateExternalRelationResolver({ fieldName, many: true })(
          parent,
          { delete: del },
          context,
          info
        );
    }
  }
  if (_disconnect) {
    for (let disconnect of _disconnect) {
      await ${typeName}UpdateExternalRelationResolver({ fieldName, many: true })(
          parent,
          { disconnect },
          context,
          info
        );
    }
  }
  return data;
};
`;

  const relationshipResolvers = `
import {${typeName}} from "../../../mongo/models/${typeName}"
import {
  get${typeName}Sorts,
  get${typeName}Where
  } from "./${typeName}InputHelpers"; 

export const create${typeName}RelationshipResolver = async ({
  fieldName,
  parent,
  create: data,
  many,
  allowDups,
  context,
  info,
}) => {
  const document = await ${typeName}.gqlCreate(data, {context,info}, {save:false});
  if (many) {
    if (allowDups) {
      parent.get(fieldName).push(document.id);
    }else {
      parent.get(fieldName).addToSet(document.id);
    }
  }else {
    parent.set(fieldName, document.id);
  }
  return {}
};

export const update${typeName}RelationshipResolver = async ({
  fieldName,
  update: {
    where: { id },
    data,
  },
  parent,
  context,
  info,
}) => {
  let record = await ${typeName}.findOne({ _id: id });
  await record.gqlUpdate(data);
  return {};
};

export const connect${typeName}RelationshipResolver = async ({
  fieldName,
  parent,
  connect: { id },
  many,
  allowDups,
  context,
  info,
}) => {
  const record = await ${typeName}.findOne({ _id: id });
  if (!record) {
    throw new Error(\`Unable to connect ${typeName}:\${id} as it was not found.\`);
  }
  if (many) {
    if (allowDups) {
      parent.get(fieldName).push(id);
    }else {
      parent.get(fieldName).addToSet(id);
    }
  }else {
    parent.set(fieldName, id);
  }
  return {}
};

export const disconnect${typeName}RelationshipResolver = async ({
  fieldName,
  disconnect,
  many,
  parent,
  allowDups,
  context,
  info,
}) => {
  if (many) {
      const { id } = disconnect;
      parent.get(fieldName).pull(id);
  }else {
    parent.set(fieldName, undefined);
  }
  return {}
};

export const delete${typeName}RelationshipResolver = async ({
  fieldName,
  delete: del,
  many,
  parent,
  allowDups,
  context,
  info,
}) => {
  if (many) {
    const {
      where: { id },
    } = del;
    await ${typeName}.deleteOne({ _id: id });
    parent.get(fieldName).pull(id);
  } else {
    const id = parent.get(fieldName);
    if (id) {
      await ${typeName}.deleteOne({ _id: id });
    }
    await ${typeName}.deleteOne({ _id: id });
    parent.set(fieldName, undefined);
  }
  return {};
};



export const ${typeName}DataLoader = ${typeName}.makeDataLoader();

export const ${typeName}RelationResolver = ({dbName, _cls}) => async (parent, args, context, info)=>{
  let options = {_cls};
  return ${typeName}DataLoader.byId(parent[dbName], options);
};

export const ${typeName}ManyRelationResolver = ({dbName, _cls}) => async (parent, args, context, info)=>{
  let options = {_cls};
  return ${typeName}DataLoader.byIds(parent[dbName], options);
  
};

export const ${typeName}CreateRelationResolver = ({ fieldName, many }) => async (
  parent,
  { connect, create },
  context,
  info
) => {
  if (create) {
    await create${typeName}RelationshipResolver({
        fieldName,
        many,
        parent,
        create,
        context,
        info
      })
  }
  if(connect) {
    await connect${typeName}RelationshipResolver({
        fieldName,
        many,
        parent,
        connect,
        context,
        info
      });
  }
      return {};
};

export const ${typeName}CreateManyRelationResolver = ({ fieldName }) => async (
  parent,
  { create: _create, connect: _connect },
  context,
  info
) => {
  if (_connect) {
    for (let connect of _connect) {
      await ${typeName}CreateRelationResolver({ fieldName, many: true })(
          parent,
          { connect },
          context,
          info
        )
    }
  }
  if (_create) {
    for (let create of _create) {
      await ${typeName}CreateRelationResolver({ fieldName, many: true, update })(
          parent,
          { create },
          context,
          info
        )
    }
  }
  return {}
};

export const ${typeName}UpdateRelationResolver = ({ fieldName, many }) => async (
  parent,
  { create, connect, update, delete: del, disconnect },
  context,
  info
) => {
  let data = {};

  if ((create || connect) && (del || disconnect)) {
    throw new Error(
      "Adding and removing documents at the same time is unsupported"
    );
  }
  if (create) {
    await create${typeName}RelationshipResolver({
        fieldName,
        many,
        parent,
        create,
        context,
        info
      })
  }
  if (connect) {
    await connect${typeName}RelationshipResolver({
        fieldName,
        many,
        parent,
        connect,
        context,
        info,
      })
  }
  if (update) {
    await update${typeName}RelationshipResolver({
        fieldName,
        many,
        parent,
        update,
        context,
        info,
      })
  }
  if (del) {
    await delete${typeName}RelationshipResolver({
        fieldName,
        many,
        parent,
        delete: del,
        context,
        info,
      })
  }

  if (disconnect) {
    await disconnect${typeName}RelationshipResolver({
        fieldName,
        many,
        parent,
        disconnect,
        context,
        info,
      })
  }
  return {};
};

export const ${typeName}UpdateManyRelationResolver = ({ fieldName }) => async (
  parent,
  {
    create: _create,
    connect: _connect,
    delete: _del,
    disconnect: _disconnect,
    update: _update,
  },
  context,
  info
) => {
  if ((_create || _connect) && (_del || _disconnect)) {
    throw new Error(
      "Adding and removing documents at the same time is unsupported"
    );
  }

  if (_connect) {
    for (let connect of _connect) {
      await ${typeName}UpdateRelationResolver({ fieldName, many: true })(
          parent,
          { connect },
          context,
          info
        )
    }
  }
  if (_create) {
    for (let create of _create) {
      await ${typeName}UpdateRelationResolver({ fieldName, many: true })(
          parent,
          { create },
          context,
          info
        )
    }
  }
  if (_update) {
    for (let update of _update) {
      await ${typeName}UpdateRelationResolver({ fieldName, many: true })(
          parent,
          { update },
          context,
          info
        )
    }
  }
  if (_del) {
    for (let del of _del) {
      await ${typeName}UpdateRelationResolver({ fieldName, many: true })(
          parent,
          { delete: del },
          context,
          info
        );
    }
  }
  if (_disconnect) {
    for (let disconnect of _disconnect) {
      await ${typeName}UpdateRelationResolver({ fieldName, many: true })(
          parent,
          { disconnect },
          context,
          info
        );
    }
  }
  return data;
};


export const ${typeName}SubQuery = ({fieldName})=> async (where, context, info)=> {
    return ${typeName}.gqlFind(where, {_id:1}, context, info).then(${plural}=>{
    const ids = ${plural}.map(({_id})=>_id);
      return {[fieldName]:{$in:ids}};
    });
    
};
  `;

  const response = `
import {${typeName}} from "../../../mongo/models/${typeName}"
import {get${typeName}Where, get${typeName}Sorts} from "./${typeName}InputHelpers"
  
export default {
Query:{
  ${single} : async (parent, {where={}, skip=0, sort:sortOptions = []}, context, info) => {
    const sort = get${typeName}Sorts(sortOptions);
    const selector = where.id ? {_id:where.id} : {};
    return ${typeName}.findOne(selector,null, {skip,sort});
  },
  ${plural}: async (parent, {where, skip, first:limit, sort:sortOptions=[], orderBy}, context, info) => {
    if(orderBy) {
      sortOptions = [orderBy];
    }
    return ${typeName}.gqlFind(where, {skip, limit, sort:sortOptions})
    
    
  },
  ${plural}Paged: async (parent, {where:passedWhere={}, skip, first:limit, sort:sortOptions=[], orderBy}, context, info) => {
    if(orderBy) {
      sortOptions = [orderBy];
    }
    const where = await get${typeName}Where(${typeName}, passedWhere, context, info);
    const sort = get${typeName}Sorts(sortOptions);
    const count = await ${typeName}.count(where);
    const total = Math.ceil(count / limit);
    const page = (skip/limit) - 1;
    const ${plural} = ${typeName}.find(where, null, {skip, limit, sort})
    return {total, hasMore:page+1 !== total, cursor: {skip, first:limit}, ${plural}};
  },
  ${single}Aggregation: async (parent, {pipeline}, context, info) => {
    return ${typeName}.aggregate(pipeline);
  }
}, 
Mutation: {
  create${typeName}: async (parent, {data}, context, info) => await ${typeName}.gqlCreate(data, {context,info}),
  update${typeName}: async (parent, {where:{id}, data}, context, info) => {
    const record = await ${typeName}.findOne({_id:id});
    return await record.gqlUpdate(data)
  },
  delete${typeName}: async (parent, {where:{id}}, context, info) => {
    return ${typeName}.findOneAndDelete({_id:id});
  }
}
}
`;
  return TemplateObject(response, [
    { response: WhereMapFile, path: "input-maps/where.js" },
    { response: SortMapFile, path: "input-maps/sort.js" },
    { response: resolversFile, path: `${typeName}Resolvers.js` },
    { response: CreateMapFile, path: "input-maps/create.js" },
    { response: UpdateMapFile, path: "input-maps/update.js" },
    {
      response: relationshipResolvers,
      path: `${typeName}RelationshipResolvers.js`,
    },
    {
      response: externalRelationshipResolvers,
      path: `${typeName}ExternalRelationshipResolvers.js`,
    },
    { response: inputHelpers, path: `${typeName}InputHelpers.js` },
  ]);
};
