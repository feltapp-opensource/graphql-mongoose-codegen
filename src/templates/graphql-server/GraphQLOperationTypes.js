import {
  buildCreateList,
  buildSortList,
  buildUpdateList,
  buildWhereList,
  TemplateObject,
} from "../../utiils";
import * as inflection from "inflected";
export const GraphQLOperationTypes = (typeName, info, typeMap) => {
  const single = inflection.camelize(typeName, false);
  const plural = inflection.pluralize(inflection.camelize(typeName, false));

  const [whereList] = buildWhereList(typeName, info, typeMap);
  const [createList] = buildCreateList(typeName, info, typeMap);
  const [updateList] = buildUpdateList(typeName, info, typeMap);
  const sortList = buildSortList(typeName, info, typeMap);

  const response = `input ${typeName}WhereUniqueInput {
    id : ObjectID!
  }
  
  input ${typeName}WhereInput {
  ${whereList
    .map(([fieldName, { inputType }]) => {
      return `${fieldName}: ${inputType}`;
    })
    .join("\n")}
  }
  
  input ${typeName}CreateInput {
  ${createList
    .map(([fieldName, { inputType }]) => {
      return `${fieldName}: ${inputType}`;
    })
    .join("\n")}
  }
  
  input ${typeName}UpdateInput {
  ${updateList
    .map(([fieldName, { inputType }]) => {
      return `${fieldName}: ${inputType}`;
    })
    .join("\n")}
  }
  
  input ${typeName}CreateOneRelationInput {
    create: ${typeName}CreateInput
    connect: ${typeName}WhereUniqueInput
  }
  
  input ${typeName}UpdateOneRelationInput {
    create: ${typeName}CreateInput
    connect: ${typeName}WhereUniqueInput
    update: ${typeName}UpdateInput
    disconnect: Boolean
    delete: Boolean
  }
  
  input ${typeName}CreateManyRelationInput {
    create: [${typeName}CreateInput!]
    connect: [${typeName}WhereUniqueInput!]
    order: [${typeName}WhereUniqueInput!]
  }
  
  input ${typeName}UpdateManyRelationUpdateManyInput {
    where: ${typeName}WhereUniqueInput
    data: ${typeName}UpdateInput
  }
  
  input ${typeName}UpdateManyRelationInput {
    create: [${typeName}CreateInput]
    connect: [${typeName}WhereUniqueInput]
    update: [${typeName}UpdateManyRelationUpdateManyInput]
    disconnect: [${typeName}WhereUniqueInput]
    delete: [${typeName}WhereUniqueInput]
    order: [${typeName}WhereUniqueInput!]
  }
  
  enum ${typeName}OrderByInput {
  ${sortList
    .map(([fieldName, field]) => {
      return `\t${fieldName}`;
    })
    .join("\n")}
  }
  type ${typeName}Pagination {
    cursor: Cursor!
    hasMore: Boolean!
    total: Int!
    ${plural}: [${typeName}!]
  }
`;
  return TemplateObject(response);
};

export const GraphQLEmbeddedOperationTypes = (typeName, info, typeMap) => {
  const [whereList] = buildWhereList(typeName, info, typeMap);
  const [createList] = buildCreateList(typeName, info, typeMap);
  const sortList = buildSortList(typeName, info, typeMap);

  const response = `input ${typeName}WhereInput {
  ${whereList
    .map(([fieldName, { inputType }]) => {
      return `${fieldName}: ${inputType}`;
    })
    .join("\n")}
  }
  
  input ${typeName}Input {
  ${createList
    .map(([fieldName, { inputType }]) => {
      return `${fieldName}: ${inputType}`;
    })
    .join("\n")}
  }
  
  enum ${typeName}OrderByInput {
  ${sortList
    .map(([fieldName, field]) => {
      return `\t${fieldName}`;
    })
    .join("\n")}
  }
`;
  return TemplateObject(response);
};
