import {
  buildExtendedFields,
  getImplementations,
  TemplateObject,
} from "../../utiils";

export const GraphQLTypeExtension = (typeName, info, typeMap) => {
  const fieldList = buildExtendedFields(typeName, info, typeMap);
  let response;
  if (Object.keys(typeMap.inherited).includes(typeName)) {
    response = `interface ${typeName}{
    ${fieldList
      .map(
        ([fieldName, type, fieldArgs]) => `${fieldName}${fieldArgs} : ${type}`
      )
      .join("\n")}
    }`;
  } else {
    const i = getImplementations(typeName, info, typeMap);
    const implementations = i.length ? ` implements ${i.join(" & ")}` : "";
    response = `type ${typeName}${implementations}{
    ${fieldList
      .map(
        ([fieldName, type, fieldArgs]) => `${fieldName}${fieldArgs} : ${type}`
      )
      .join("\n")}
  }
`;
  }

  return TemplateObject(response);
};
