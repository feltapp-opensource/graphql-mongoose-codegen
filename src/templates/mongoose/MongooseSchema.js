import { getMongooseSchemaTemplateFields, TemplateObject } from "../../utiils";

export const MongooseSchemaTemplate = (typeName, info, typeMap) => {
  const getDiscriminator = (typeName) => {
    let models = [];
    let node;
    if (Object.keys(typeMap?.inherited).includes(typeName)) {
      node = typeMap.inherited[typeName];
    } else if (Object.keys(typeMap?.abstracts).includes(typeName)) {
      node = typeMap.abstracts[typeName];
    } else if (Object.keys(typeMap?.models).includes(typeName)) {
      node = typeMap.models[typeName];
    }

    if (node?.inherits && node?.from) {
      models = [...getDiscriminator(node?.from), typeName];
    }
    return models;
  };

  const [fields, imports] = getMongooseSchemaTemplateFields(
    info.fields,
    typeMap
  );
  let options = { collection: info.collection };
  let schemaDeclaration = "new mongoose.Schema";
  let modelCreate = `mongoose.model('${typeName}', ${typeName}Schema)`;
  if (info.inherits) {
    options = { discriminatorKey: "_cls" };
    const discriminators = getDiscriminator(typeName);
    if (discriminators.length === 2) {
      imports.add(`import {${info.from}} from "./${info.from}"`);
    } else {
      imports.add(`import {extend${info.from}} from "./${info.from}"`);
      imports.add(
        `import {${discriminators[0]}} from "./${discriminators[0]}"`
      );
      schemaDeclaration = `extend${info.from}`;
    }

    modelCreate = `${discriminators[0]}.discriminator("${discriminators.join(
      "."
    )}",${typeName}Schema)`;
  } else if (info.from) {
    imports.add(
      `import {extend${info.from}} from "../interfaces/${info.from}"`
    );
    schemaDeclaration = `extend${info.from}`;
  }
  const response = `import Hash from "object-hash";
  import DataLoader from "dataloader";
  import mongoose from "mongoose";
  import {ObjectID} from "mongodb";
  import ${typeName}CreateMap from "../../graphql/${typeName}/js/input-maps/create";
  import ${typeName}UpdateMap from "../../graphql/${typeName}/js/input-maps/update";
  import {
  get${typeName}Sorts,
  get${typeName}Where,
} from "../../graphql/${typeName}/js/${typeName}InputHelpers"; 
  ${Array.from(imports).join("\n")}

class ${typeName}GraphQLMethods {
  static async gqlCreate(data, {context, info}={}, {save=true}={}) {
    const document = new this();
    const properties = Object.entries(data);
    if (!document._id) {
      document._id = ObjectID();
    }
    for (let i = 0; i < properties.length; i++) {
      const [key, value] = properties[i];
      const props = await ${typeName}CreateMap[key](document, value, context, info);
      Object.entries(props).forEach(([fieldName, createValue]) => {
        document.set(fieldName, createValue);
      });
    }
    if(save) {
      await document.save();
    }
    return document;
  }
  
  static async gqlFind(where = {}, options = {}, context, info) {
    const { skip, first: limit, sort: sortOptions } = options;
    const sort = get${typeName}Sorts(sortOptions);
    return this.find(
      await get${typeName}Where(where, context, info),
      null,
      { skip, limit, sort }
    );
  }
  
  static makeDataLoader() {
  const _loaders = {};
  const byId = (id, selector) => {
    if(!id) {
      return null;
    }
    const hash = Hash(selector);
    if(!_loaders[hash]) {
      _loaders[hash] = new DataLoader(async keys =>{
        const records = await this.find({_id: {$in: keys}});
        return keys.map(_id => {
          const record = records.find(r=>r._id.toString() === _id.toString());
          return record || null;
        });
      });
    }
    return _loaders[hash].load(id);
  };
  const byIds = (ids, selector) => {
    return Promise.all(ids.map(id=>byId(id,selector)));
  };
  
  const byExternalId = (key, id, selector, many) => {
    if(!id) {
      return null;
    }
    const hash = Hash({...selector, key, id});
    if(!_loaders[hash]) {
      _loaders[hash] = new DataLoader(async keys =>{
        const records = await this.find({[key]: {$in: keys}});
        return keys.map(_id => {
          if(many) {
            return records.filter(r=>r.get(key).toString() === _id.toString());
          }
          const record = records.find(r=>r.get(key).toString() === _id.toString());
          return record || null;
        });
      });
    }
    return _loaders[hash].load(id);
  };
  
  return {byId,byIds, byExternalId}; 
};

  async gqlUpdate(data) {
    const updates = Object.entries(data);
    const document = this;
    for (let i = 0; i < updates.length; i++) {
      const [key, value] = updates[i];
      const props = await ${typeName}UpdateMap[key](document, value);
      Object.entries(props).forEach(([fieldName, createValue]) => {
        document.set(fieldName, createValue);
      });
    }
    document.save()
    return document;
  }
}   
export const ${typeName}Schema = ${schemaDeclaration}({
${fields}
},${JSON.stringify(options)});

${typeName}Schema.loadClass(${typeName}GraphQLMethods);

export const ${typeName} = ${modelCreate};
`;

  return TemplateObject(response);
};
