import { getMongooseSchemaTemplateFields, TemplateObject } from "../../utiils";

export const MongooseEmbeddedSchemaTemplate = (typeName, info, typeMap) => {
  const [fields, imports] = getMongooseSchemaTemplateFields(
    info.fields,
    typeMap
  );
  const response = `import mongoose from "mongoose";
  ${Array.from(imports).join("\n")}
export const ${typeName}Schema = new mongoose.Schema({
${fields}
},{_id:false});

export const ${typeName} = mongoose.model('${typeName}', ${typeName}Schema);
`;
  return TemplateObject(response);
};
