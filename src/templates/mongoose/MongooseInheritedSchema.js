import { getMongooseSchemaTemplateFields, TemplateObject } from "../../utiils";
import { singularize, underscore } from "inflected";

export const MongooseInheritedSchemaTemplate = (typeName, info, typeMap) => {
  const getDiscriminator = (typeName) => {
    let models = [];
    let node;
    if (Object.keys(typeMap?.inherited).includes(typeName)) {
      node = typeMap.inherited[typeName];
    } else if (Object.keys(typeMap?.abstracts).includes(typeName)) {
      node = typeMap.abstracts[typeName];
    }

    if (node?.inherits && node?.from) {
      models = [...getDiscriminator(node?.from), typeName];
    }
    return models;
  };

  const [fields, imports] = getMongooseSchemaTemplateFields(
    info.fields,
    typeMap
  );

  let { abstract, from, collection } = info;

  if (from) {
    if (Object.keys(typeMap?.abstracts || []).includes(from)) {
      abstract = true;
    }
  }

  const extendModel = `mongoose.model`;
  const options = { discriminatorKey: "_cls"};
  if (abstract) {
    options.collection = collection;
  }

  const extendSchema = `extend${from}`;
  const inheritSchema = `new mongoose.Schema`;

  const inheritExport = `export const extend${typeName} = (schemaFields, options = {}) => {
  const newSchema = new mongoose.Schema(
    Object.assign({}, ${typeName}Schema.obj, schemaFields),
    options
  );
  return newSchema;
};`;
  const abstractExport = `export const ${typeName} = ${extendModel}("${typeName}", ${typeName}Schema);`;
  if (abstract) {
    imports.add(`import { extend${from} } from "../interfaces/${from}";`);
  }

  const response = `import mongoose from "mongoose";
  ${Array.from(imports).join("\n")}
  export const ${typeName}Schema = ${
    abstract ? extendSchema : inheritSchema
  }({${fields}},${JSON.stringify(options)});
  
  ${abstract ? abstractExport : inheritExport}
  `;

  return TemplateObject(response);
};
