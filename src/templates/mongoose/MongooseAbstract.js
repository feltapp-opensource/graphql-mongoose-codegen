import { getMongooseSchemaTemplateFields, TemplateObject } from "../../utiils";

export const MongooseInterfaceSchemaTemplate = (typeName, info, typeMap) => {
  const [fields, imports] = getMongooseSchemaTemplateFields(
    info.fields,
    typeMap
  );
  let schemaDefinition = "new mongoose.Schema";
  if (info.from) {
    imports.add(`import {extend${info.from}} from "./${info.from}"`);
    schemaDefinition = `extend${info.from}`;
  }

  const response = `
import mongoose from "mongoose";
import MongooseConfig from "../../../MongooseConfig";
${Array.from(imports).join("\n")}

const ${typeName}Schema = ${schemaDefinition}({
  ${fields}
});

export const extend${typeName} = (schemaFields, options = {}) => {
  const newSchema = new mongoose.Schema(
    Object.assign({}, ${typeName}Schema.obj, schemaFields),
    options
  );
  return newSchema;
};
export default ${typeName}Schema;
`;
  return TemplateObject(response);
};
