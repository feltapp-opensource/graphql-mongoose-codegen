export default {
  hooks: {
    Account: {
      pre: {},
      post: {},
    },
  },
  defaults: {
    Account: {
      subscribedToEmails: async (parent, args, context, info) => true,
    },
  },
};
