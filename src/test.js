import mongoose from "mongoose";
import { Accordion } from "./generated/mongo/models/Accordion";

const runTest = async () => {
  await mongoose.connect(process.env.MONGO_URL, {
    dbName: process.env.MONGO_DB,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const a = await Accordion.findOne({ _id: "5a79a5adedc906000c9a8392" });

  console.log(a.toJSON());
};

runTest().then();
